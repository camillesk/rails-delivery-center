# frozen_string_literal: true

class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer :external_code
      t.integer :store_id
      t.float :sub_total
      t.float :delivery_fee
      t.float :total_shipping
      t.float :total
      t.string :country
      t.string :state
      t.string :city
      t.string :district
      t.string :street
      t.string :complement
      t.float :latitude
      t.float :longitude
      t.datetime :dt_order_create
      t.integer :postal_code
      t.integer :number
      t.belongs_to :customer

      t.timestamps
    end
  end
end
