# frozen_string_literal: true

class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.string :external_code
      t.string :name
      t.float :price
      t.integer :quantity
      t.float :total
      t.string :sub_items, array: true
      t.belongs_to :order
      t.belongs_to :customer

      t.timestamps
    end
  end
end
