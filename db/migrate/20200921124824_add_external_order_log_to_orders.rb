class AddExternalOrderLogToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :external_order_log, :text
  end
end
