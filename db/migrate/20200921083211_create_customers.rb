# frozen_string_literal: true

class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|
      t.integer :external_code
      t.string :name
      t.string :email
      t.string :contact

      t.timestamps
    end
  end
end
