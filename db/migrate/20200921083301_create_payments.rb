# frozen_string_literal: true

class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.string :payment_type
      t.float :value
      t.belongs_to :order
      t.belongs_to :customer

      t.timestamps
    end
  end
end
