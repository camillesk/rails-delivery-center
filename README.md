## Delivery Center Ruby Back-end Test
Data: 21/09/20

O projeto tem como objetivo parsear um objeto JSON enviado via request para o endpoint `order/new`, a aplicação irá então fazer algumas validações e parsear o mesmo para que seja enviado para
 *https://delivery-center-recruitment-ap.herokuapp.com/* seguindo os critérios do teste.

Passo a passo para rodar o projeto:  (_É necessário ter o ruby e o postgresql instalados na máquina_)

**No terminal**

> sudo systemctl start postgresql

> bundle install

> rails db:create

> rails db:migrate

> rails s

**Usando Postman ou Insomnia**

Para criar um novo pedido, que será enviado e salvo no banco de dados:

> Envie uma request do tipo POST para o endpoint `localhost:3000/orders` com o JSON desejado.

*Caso esteja tudo correto, a API retornará um status 200. Caso haja algum erro, o mesmo será retornado.*

* Para verificar os pedidos existentes no banco de dados:

> Envie uma request do tipo GET para `localhost:3000/orders`

*O payload original enviado fica salvo no campo _external_order_log_ dentro do pedido*
