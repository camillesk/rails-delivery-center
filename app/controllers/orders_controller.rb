# frozen_string_literal: true

class OrdersController < ApplicationController
  before_action :set_order, only: %i[show update destroy]

  # GET /orders
  def index
    @orders = Order.all

    render json: @orders
  end

  # GET /orders/1
  def show
    render json: @order
  end

  # POST /orders
  def create
    @order = Order.new
    json_errors = JSON::Validator.fully_validate(@order.order_schema, params.to_json)

    unless json_errors.blank?
      render json: json_errors, status: :unprocessable_entity
      return
    end

    parsed_order = @order.parse_order(params)

    headers = { 'X-Sent' => Time.now.strftime('%Hh%M - %d/%m/%y').to_s }

    response = HTTParty.post(
      'https://delivery-center-recruitment-ap.herokuapp.com/',
      body: parsed_order.to_json,
      headers: headers
    )

    if response.code.eql? 200
      @order.create_customer(params)
      @order.create_order(parsed_order)
      @order.create_items(params)
      @order.create_payments(params)
      @order.create_log(params)
      
      render json: 'Parsed and saved with success', status: 200
    else
      render json: response.body, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /orders/1
  # def update
  #   if @order.update(order_params)
  #     render json: @order
  #   else
  #     render json: @order.errors, status: :unprocessable_entity
  #   end
  # end

  # DELETE /orders/1
  def destroy
    @order.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = Order.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def order_params
    params.require(:order).permit(:external_code, :store_id, :sub_total, :delivery_fee, :total, :country, :state,
                                  :city, :district, :street, :complement, :latitude, :longitude, :dt_order_create,
                                  :postal_code, :number)
  end
end
