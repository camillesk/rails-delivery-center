# frozen_string_literal: true

class ExternalOrder < ApplicationRecord
  # Validations
  validates :id, presence: true
  validates :store_id, presence: true
  validates :total_amount, presence: true
  validates :total_shipping, presence: true
  validates :total_amount_with_shipping, presence: true
  validates :date_created, presence: true
  # validates_associated :books

  def parse(json_params)
    order = {
      "externalCode": json_params['id'],
      "storeId": json_params['store_id'],
      "subTotal": json_params['total_amount'],
      "deliveryFee": json_params['total_shipping'],
      "total_shipping": json_params['total_shipping'],
      "total": json_params['total_amount_with_shipping'],
      "postalCode": json_params['shipping']['receiver_address']['zip_code'],
      "country": json_params['shipping']['receiver_address']['country']['id'],
      "state": json_params['shipping']['receiver_address']['state']['name'],
      "city": json_params['shipping']['receiver_address']['city']['name'],
      "district": json_params['shipping']['receiver_address']['neighborhood']['name'],
      "street": json_params['shipping']['receiver_address']['street_name'],
      "number": json_params['shipping']['receiver_address']['street_number'],
      "complement": json_params['shipping']['receiver_address']['comment'],
      "latitude": json_params['shipping']['receiver_address']['latitude'],
      "longitude": json_params['shipping']['receiver_address']['longitude'],
      "dtOrderCreate": json_params['date_created'],
      "customer": {
        "externalCode": json_params['buyer']['id'],
        "name": json_params['buyer']['nickname'],
        "email": json_params['buyer']['email'],
        "contact": parse_contact(json_params)
      },
      "items": parse_items(json_params),
      "payments": parse_payments(json_params)
    }

    order
  end

  private

  def parse_items(json_params)
    items = []

    json_params['order_items'].map do |item|
      items << {
        "externalCode": item['item']['id'],
        "name": item['item']['title'],
        "price": item['unit_price'],
        "quantity": item['quantity'],
        "total": item['full_unit_price'],
        "subItems": item['sub_items'] || nil
      }
    end

    items
  end

  def parse_payments(json_params)
    payments = []

    json_params['payments'].map do |payment|
      payments << {
        "type": payment['payment_type'].upcase,
        "value": payment['total_paid_amount']
      }
    end

    payments
  end

  def parse_contact(json_params)
    "#{json_params['buyer']['phone']['area_code']}#{json_params['buyer']['phone']['number']}"
  end
end
