# frozen_string_literal: true

class Item < ApplicationRecord
  # Relationships
  belongs_to :order

  # Validations
  validates :external_code, presence: true
  validates :name, presence: true
  validates :price, presence: true
  validates :quantity, presence: true
  validates :total, presence: true
end
