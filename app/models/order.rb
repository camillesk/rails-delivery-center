# frozen_string_literal: true

class Order < ApplicationRecord
  # Relationships
  belongs_to :customer
  has_many :items
  has_many :payments

  # Validations
  validates :external_code, presence: true
  validates :store_id, presence: true
  validates :sub_total, presence: true
  validates :delivery_fee, presence: true
  validates :total_shipping, presence: true
  validates :total, presence: true
  validates :postal_code, presence: true
  validates :country, presence: true
  validates :state, presence: true
  validates :city, presence: true
  validates :district, presence: true
  validates :street, presence: true
  validates :number, presence: true
  validates :complement, presence: true
  validates :latitude, presence: true
  validates :longitude, presence: true
  validates :dt_order_create, presence: true
  validates_associated :items
  validates_associated :payments
  validates_associated :customer

  def parse_order(params)
    body = {
      "externalCode": params['id'],
      "storeId": params['store_id'],
      "subTotal": params['total_amount'],
      "deliveryFee": params['total_shipping'],
      "total_shipping": params['total_shipping'],
      "total": params['total_amount_with_shipping'],
      "postalCode": params['shipping']['receiver_address']['zip_code'],
      "country": params['shipping']['receiver_address']['country']['id'],
      "state": params['shipping']['receiver_address']['state']['name'],
      "city": params['shipping']['receiver_address']['city']['name'],
      "district": params['shipping']['receiver_address']['neighborhood']['name'],
      "street": params['shipping']['receiver_address']['street_name'],
      "number": params['shipping']['receiver_address']['street_number'],
      "complement": params['shipping']['receiver_address']['comment'],
      "latitude": params['shipping']['receiver_address']['latitude'],
      "longitude": params['shipping']['receiver_address']['longitude'],
      "dtOrderCreate": params['date_created'],
      "customer": {
        "externalCode": params['buyer']['id'],
        "name": params['buyer']['nickname'],
        "email": params['buyer']['email'],
        "contact": parse_contact(params)
      },
      "items": parse_items(params),
      "payments": parse_payments(params)
    }

    body
  end

  def order_schema
    {
      'type' => 'object',
      'required' => %w[id store_id total_amount total_shipping total_amount_with_shipping date_created],
      'properties' => {
        'shipping' => {
          'type' => 'object',
          'properties' => {
            'receiver_address' => {
              'type' => 'object',
              'required' => %w[zip_code street_name street_number comment latitude longitude],
              'properties' => {
                'country' => {
                  'type' => 'object',
                  'required' => ['id']
                },
                'state' => {
                  'type' => 'object',
                  'required' => ['name']
                },
                'city' => {
                  'type' => 'object',
                  'required' => ['name']
                },
                'neighborhood' => {
                  'type' => 'object',
                  'required' => ['name']
                }
              }
            }
          }
        },
        'buyer' => {
          'type' => 'object',
          'required' => %w[id nickname email],
          'properties' => {
            'phone' => {
              'type' => 'object',
              'required' => %w[area_code number]
            }
          }
        },
        'order_items' => {
          'type' => 'array',
          'items' => {
            'required' => %w[unit_price quantity full_unit_price],
            'properties' => {
              'item' => {
                'type' => 'object',
                'required' => %w[id title]
              }
            }
          }
        },
        'payments' => {
          'type' => 'array',
          'items' => {
            'required' => %w[payment_type total_paid_amount]
          }
        }
      }
    }
  end

  def create_order(parsed_params)
    update_attributes(
      external_code: parsed_params[:externalCode],
      store_id: parsed_params[:storeId],
      sub_total: parsed_params[:subTotal],
      delivery_fee: parsed_params[:deliveryFee],
      total_shipping: parsed_params[:total_shipping],
      total: parsed_params[:total],
      postal_code: parsed_params[:postalCode],
      country: parsed_params[:country],
      state: parsed_params[:state],
      city: parsed_params[:city],
      district: parsed_params[:district],
      street: parsed_params[:street],
      number: parsed_params[:number],
      complement: parsed_params[:complement],
      latitude: parsed_params[:latitude],
      longitude: parsed_params[:longitude],
      dt_order_create: parsed_params[:dtOrderCreate]
    )
  end

  def create_customer(params)
    c = Customer.find_or_create_by!(
      external_code: params['buyer']['id'],
      name: params['buyer']['nickname'],
      email: params['buyer']['email'],
      contact: "#{params['buyer']['phone']['area_code']}#{params['buyer']['phone']['number']}"
    )

    self.customer_id = c.id
  end

  def create_items(params)
    params[:order_items].map do |item|
      Item.new(
        external_code: item[:item][:id],
        name: item[:item][:title],
        price: item[:unit_price],
        quantity: item[:quantity],
        total: item[:full_unit_price],
        sub_items: item[:sub_items],
        order_id: id
      )
    end
  end

  def create_payments(params)
    params[:payments].map do |payment|
      Payment.new(
        payment_type: payment[:payment_type].upcase,
        value: payment[:total_paid_amount],
        order_id: id
      )
    end
  end

  def create_log(params)
    self.external_order_log = params.to_json
    self.save
  end

  private

  def parse_items(params)
    items = []

    params['order_items'].map do |item|
      items << {
        "externalCode": item['item']['id'],
        "name": item['item']['title'],
        "price": item['unit_price'],
        "quantity": item['quantity'],
        "total": item['full_unit_price'],
        "subItems": item['sub_items'] || nil
      }
    end

    items
  end

  def parse_payments(params)
    payments = []

    params['payments'].map do |payment|
      payments << {
        "type": payment['payment_type'].upcase,
        "value": payment['total_paid_amount']
      }
    end

    payments
  end

  def parse_contact(params)
    "#{params['buyer']['phone']['area_code']}#{params['buyer']['phone']['number']}"
  end
end
