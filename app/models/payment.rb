# frozen_string_literal: true

class Payment < ApplicationRecord
  # Relationships
  belongs_to :customer
  belongs_to :order

  # Validations
  validates :payment_type, presence: true
  validates :value, presence: true
end
