# frozen_string_literal: true

class Customer < ApplicationRecord
  # Relationships
  has_many :orders
  has_many :payments

  # Validations
  validates :external_code, presence: true
  validates :name, presence: true
  validates :email, presence: true
  validates :contact, presence: true
end
