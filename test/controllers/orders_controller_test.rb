# frozen_string_literal: true

require 'test_helper'

class OrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @order = orders(:one)
  end

  test 'should get index' do
    get orders_url, as: :json
    assert_response :success
  end

  test 'should create order' do
    assert_difference('Order.count') do
      post orders_url, params: {
        order: {
          city: @order.city,
          complement: @order.complement,
          country: @order.country,
          delivery_fee: @order.delivery_fee,
          district: @order.district,
          dt_order_create: @order.dt_order_create,
          external_code: @order.external_code,
          latitude: @order.latitude,
          longitude: @order.longitude,
          number: @order.number,
          postal_code: @order.postal_code,
          state: @order.state,
          store_id: @order.store_id,
          street: @order.street,
          sub_total: @order.sub_total,
          total: @order.total
        }
      }, as: :json
    end

    assert_response 201
  end

  test 'should show order' do
    get order_url(@order), as: :json
    assert_response :success
  end

  test 'should update order' do
    patch order_url(@order), params: {
      order: {
        city: @order.city,
        complement: @order.complement,
        country: @order.country,
        delivery_fee: @order.delivery_fee,
        district: @order.district,
        dt_order_create: @order.dt_order_create,
        external_code: @order.external_code,
        latitude: @order.latitude,
        longitude: @order.longitude,
        number: @order.number,
        postal_code: @order.postal_code,
        state: @order.state,
        store_id: @order.store_id,
        street: @order.street,
        sub_total: @order.sub_total,
        total: @order.total
      }
    }, as: :json
    assert_response 200
  end

  test 'should destroy order' do
    assert_difference('Order.count', -1) do
      delete order_url(@order), as: :json
    end

    assert_response 204
  end
end
